<?php
/**
 * @author: scipilot
 * @since: 06/02/2015
 */
namespace Scipilot\Mullet\Test\Tests\Storage;

use Scipilot\Mullet\Config\JsonFileConfig;
use Scipilot\Mullet\Log\LocalLog;
use Scipilot\Mullet\Model\Cloud;
use Scipilot\Mullet\Model\Tag;
use Scipilot\Mullet\Storage\PdoDbStorage;
use Scipilot\Mullet\Test\Fixtures\FixtureRepository;

class PdoDbStorageTest extends StorageTest {

	/**
	 * @var PdoDbStorage
	 */
	protected $storage;

	private static $pdo;

	public function setup(){
		parent::setup();
		$this->app->log = new LocalLog();
		//$this->FIXTURE_FILENAME = 'Storage/storage-test.json';
	}

	protected function makeTestSubject(){
		// we need the real config before making PDO
		$this->app->config = new JsonFileConfig($this->app);
		$this->storage = new PdoDbStorage($this->app);
	}

	/**
	 * @return \PDO
	 */
	protected function getPDO(){
		if(!self::$pdo){
			// todo probably makes this class/properites?
			// insert the fixture into the storage
			$confDSN   = $this->app->config->get('storage.db.pdo.dsn');
			$username        = $this->app->config->get('storage.db.pdo.username');
			$password        = $this->app->config->get('storage.db.pdo.password');
			self::$pdo = new \PDO($confDSN, $username, $password);
		}
		return self::$pdo;
	}

	protected function executeSQLFixture($sName){
		$sqlFixture = FixtureRepository::get($sName);

		$pdo = $this->getPDO();
		//$aQueries = explode(';',$sqlFixture);
		//		foreach($aQueries as $sql){ //debug: individual queries!
		$sql = $sqlFixture;
		if($pdo->query($sql) === false){
			trigger_error(sprintf('SQL query failed:%s PDO said:%s', $sql, var_export($pdo->errorInfo(), true)));
		}
		//		}
	}

	// white box tests the storage
	public function testRead(){
		// setup fixtures
		$this->executeSQLFixture('Storage/storage-test-teardown.sql');
		$this->executeSQLFixture('Storage/storage-test1-setup.sql');
		$jsonFixture = FixtureRepository::get('Storage/storage-test1.json');
		$dbExpected = json_decode($jsonFixture);

		// todo test more entities, (cloud, relation)
		$entity = 'tag';
		$actual = $this->storage->read($entity, array('id'=>1));

		$this->assertEquals($dbExpected->tag1, $actual[0]);
	}

	// white box tests the storage
	public function testCreate(){

		// setup fixtures
		$this->executeSQLFixture('Storage/storage-test-teardown.sql');
		$this->executeSQLFixture('Storage/storage-testCreate-setup.sql');
		$jsonFixture = FixtureRepository::get('Storage/storage-testCreate.json');
		$objFixture = json_decode($jsonFixture);

		// todo more entities, (cloud, relation)
		// Write through the driver
		$this->storage->create(Tag::STORAGE_ENTITY, $objFixture);

		// Verify expected results back via DB
		$sqlFixture = FixtureRepository::get('Storage/storage-testCreate-test.sql');
		$pdo = $this->getPDO();
		$st =$pdo->prepare($sqlFixture);
		$st->execute();
		$row = $st->fetchObject();

		$this->assertEquals($objFixture->tag2, $row);
	}

	// white box tests the storage
	public function testUpdate(){

		// setup fixtures
		$this->executeSQLFixture('Storage/storage-test-teardown.sql');
		$this->executeSQLFixture('Storage/storage-testUpdate-setup.sql');
		$jsonFixture = FixtureRepository::get('Storage/storage-testUpdate.json');
		$objFixture = json_decode($jsonFixture);

		// todo more entities, (cloud, relation)
		// Write through the driver
		$this->storage->update(Tag::STORAGE_ENTITY, $objFixture);

		// Verify expected results back via DB
		$sqlFixture = FixtureRepository::get('Storage/storage-testUpdate-test.sql');
		$pdo = $this->getPDO();
		$st =$pdo->prepare($sqlFixture);
		$st->execute();
		$row = $st->fetchObject();

		$this->assertEquals($objFixture->tag2, $row);
	}

	// white box tests the storage
	public function testDelete(){
		// setup fixtures
		$this->executeSQLFixture('Storage/storage-test-teardown.sql');
		$this->executeSQLFixture('Storage/storage-test3-setup.sql');
		$jsonFixture = FixtureRepository::get('Storage/storage-test3.json');
		$objFixture = json_decode($jsonFixture);

		// Delete the first
		$this->storage->delete(Tag::STORAGE_ENTITY, array('id' =>1));
		$this->storage->delete(Cloud::STORAGE_ENTITY, array('id' =>1));

		// Positive test
		$actual = $this->storage->read(Tag::STORAGE_ENTITY, array('id' =>1));
		$this->assertEmpty($actual);
		$actual = $this->storage->read(Cloud::STORAGE_ENTITY, array('id' =>1));
		$this->assertEmpty($actual);

		// Negative test - ensure nothing else is deleted
		$actual = $this->storage->read(Tag::STORAGE_ENTITY, array('id' =>2));
		$this->assertEquals($objFixture->tag2, $actual[0]);
		$actual = $this->storage->read(Cloud::STORAGE_ENTITY, array('id' =>2));
		$this->assertEquals($objFixture->cloud2, $actual[0]);

		// Delete the rest
		$this->storage->delete(Tag::STORAGE_ENTITY, array('id' =>2));
		$this->storage->delete(Cloud::STORAGE_ENTITY, array('id' =>2));

		// Positive test
		$actual = $this->storage->read(Tag::STORAGE_ENTITY, array('id' =>2));
		$this->assertEmpty($actual);
		$actual = $this->storage->read(Cloud::STORAGE_ENTITY, array('id' =>2));
		$this->assertEmpty($actual);


		// todo test more entities, ( relation)
	}

}
