<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Tests\Storage;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\Storage\IStorage;
use Scipilot\Mullet\Test\Mocks\NullMockContainer;
use Scipilot\Mullet\Test\Tests\BaseTest;

abstract class StorageTest extends BaseTest {

	/**
	 * @var IStorage
	 */
	protected $storage;

	// Covers both read and write
	public function testReadWrite(){
		$this->markTestSkipped();
		//		$this->storage->write($entity, $id);
		//
		//		$row2 = $this->storage->read(1);
		//
		//		$this->assertEquals($row1, $row2);
	}

}
