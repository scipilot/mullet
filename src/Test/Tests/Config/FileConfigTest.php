<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Config;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\config\IConfig;
use Scipilot\Pulse\Test\Fixtures\FixtureRepository;
use Scipilot\Pulse\Test\Mocks\NullMockContainer;

abstract class FileConfigTest extends \PHPUnit_Framework_TestCase {
	protected $FIXTURE_FILENAME = 'x';//overridden by daughter
	private static $TMP_PATH;

	const TEST_KEY1 = 'testkey1';
	const TEST_VALUE1 = 'testvalue1';
	const TEST_KEY2 = 'testkey2';
	const TEST_VALUE2 = 'testvalue2';

	/**
	 * @var Container
	 */
	protected $container;
	/**
	 * @var IConfig
	 */
	protected $config;

	public static function setUpBeforeClass(){
		self::$TMP_PATH = __DIR__.'/../../../../storage/';
	}

	// SETUP SHOULD BE CALLED IN THE DAUGHTER CLASSES AND SET $config TO THE SPECIFIC CLASS
	public function setup(){
		$this->container = new NullMockContainer();
	}

	// this should setup the $config
	protected abstract function makeTestSubject();

		// Covers both read and write
	public function testGetSet(){
		$valExpected = self::TEST_VALUE1;
		$this->config->set(self::TEST_KEY1, $valExpected, true);
		$valActual = $this->config->get(self::TEST_KEY1);

		$this->assertEquals(self::TEST_VALUE1, $valActual);

		// negative test, check other keys not affected
		$valActual = $this->config->get(self::TEST_KEY2);
		$this->assertNotEquals($valExpected, $valActual);

	}

	// white box tests the config file
	public function testGet() {
		$fixture = FixtureRepository::get($this->FIXTURE_FILENAME);
		$dbExpected = $this->decode($fixture);

		// insert the fixture into the tmp folder
		file_put_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME, $fixture);
		$valActual = $this->config->get(self::TEST_KEY1);

		$this->assertEquals($dbExpected->{self::TEST_KEY1}, $valActual);
	}

	// white box tests the config file
	public function testSet() {
		$fbFileExpected = FixtureRepository::get($this->FIXTURE_FILENAME);
		$dbExpected = $this->decode($fbFileExpected);

		// write and then fetch the stored result from the tmp folder
		$this->config->set(self::TEST_KEY1, $dbExpected->{self::TEST_KEY1}, true);
		$dbFileActual = file_get_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME);

		$this->assertEquals($fbFileExpected, $dbFileActual);
	}

	// Similar to SetGet, but with an intevening reconstruction
	public function testPersistence(){
		// store a new unique random value
		$valExpected = rand(0,999999);
		$this->config->set(self::TEST_KEY2, $valExpected, true);

		// destroy memory cache
		$this->makeTestSubject();

		$valActual = $this->config->get(self::TEST_KEY2);
		$this->assertEquals($valExpected, $valActual);

		// negative test, check other keys not affected
		$valActual = $this->config->get(self::TEST_KEY1);
		$this->assertNotEquals($valExpected, $valActual);
	}

}
