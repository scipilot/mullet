<?php
namespace Scipilot\Mullet\Test\Tests;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\Test\Mocks\NullMockContainer;

/**
 * @author: scipilot
 * @since: 5/12/2014
 */
abstract class BaseTest extends \PHPUnit_Framework_TestCase {

	protected $FIXTURE_FILENAME = 'x';//overridden by daughter
	protected static $TMP_PATH;

	/**
	 * @var Container
	 */
	protected $app;

	public static function setUpBeforeClass(){
		self::$TMP_PATH = __DIR__.'/../../../../storage/';
	}

	protected function setup(){
		$this->app = new NullMockContainer();
		$this->makeTestSubject();
	}

	// this should setup the System Under Test.
	protected abstract function makeTestSubject();

}
