<?php
namespace Scipilot\Mullet\Test\Tests\Model;

use Scipilot\Mullet\Model\Cloud;
use Scipilot\Mullet\Model\Tag;
use Scipilot\Mullet\Test\Mocks\TestContainer;
use Scipilot\Mullet\Test\Tests\BaseTest;

/**
 */
class TagTest extends BaseTest {
	/**
	 * @var Tag
	 */
	protected $object;

	protected $cloudModel;
	protected $cloudId;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp(){
		//parent::setup(); I want to override the container!

		$this->app 		= new TestContainer();
		$this->object = new Tag($this->app);

		// Create a new cloud
		$t = 'UnitTestCloud-'.rand(0,9999);
		// have to create a cloud first
		$this->cloudModel = new Cloud($this->app);
		$this->cloudId = $this->cloudModel->create($t);
		// DevNote: asserting in setup causes any 'i'ncomplete tests to be marked as passed!
		$this->assertNotNull($this->cloudId);
		$this->assertGreaterThan(0, intval($this->cloudId), "c=".$this->cloudId);
	}

	protected function makeTestSubject(){
		// TODO: Implement makeTestSubject() method.
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown(){
		$this->object = null;
	}

	private function makeRandomTag($name){
		$t = 'UnitTest-Tag'.$name.'-'.rand(0, 9999);

		$i = $this->object->create($t, $this->cloudId);
		$this->assertGreaterThan(0, intval($i));

		return [$i, $t];
	}

	// TESTS ------------------------------------------------

	/**
	 * @covers create
	 * @covers find
	 */
	public function testCreate(){
		list($i, $t) = $this->makeRandomTag('Create');

		$t = $this->object->find($t, $this->cloudId);
		$this->assertEquals($i, $t->id);
	}

	public function testFind(){
		// covered in create!
	}

	public function testIn(){
		list($i, $t) = $this->makeRandomTag('In');

		$b = $this->object->in($t, $this->cloudId);
		$this->assertTrue($b);
	}

	public function testId(){
		list($i, $t) = $this->makeRandomTag('Id');

		$i2 = $this->object->id($t, $this->cloudId);
		$this->assertEquals($i, $i2);

		// negative test covered by delete
	}

	public function testUpdate(){
		list($i, $t1) = $this->makeRandomTag('Update');
		$t2 = 'UnitTest-TagUpdate-'.rand(0, 9999);

		$b = $this->object->update($t1, $this->cloudId, $t2);
		$this->assertTrue($b);

		$i2 = $this->object->id($t2, $this->cloudId);
		$this->assertEquals($i, $i2);
	}

	public function testDelete(){
		list($i, $t) = $this->makeRandomTag('Delete');

		$b = $this->object->delete($i, $this->cloudId);
		$this->assertTrue($b);

		$i2 = $this->object->id($t, $this->cloudId);
		$this->assertNull($i2);
		$b = $this->object->in($t, $this->cloudId);
		$this->assertFalse($b);
	}

}
