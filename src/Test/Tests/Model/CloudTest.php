<?php
namespace Scipilot\Mullet\Test\Tests\Model;

use Scipilot\Mullet\Model\Cloud;
use Scipilot\Mullet\Model\Tag;
use Scipilot\Mullet\Test\Mocks\TestContainer;
use Scipilot\Mullet\Test\Tests\BaseTest;

/**
 */
class CloudTest extends BaseTest {
	/**
	 * @var Cloud
	 */
	protected $object;

	protected $tagModel;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp(){
		//parent::setup(); I want to override the container!

		$this->app 		= new TestContainer();
		$this->object = new Cloud($this->app);

	}

	protected function makeTestSubject(){
		// TODO: Implement makeTestSubject() method.
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown(){
		$this->object = null;
	}

	private function makeRandomCloud($name){
		$n = 'UnitTest-Cloud'.$name.'-'.rand(0, 9999);

		$i = $this->object->create($n);
		$this->assertGreaterThan(0, intval($i));

		return [$i, $n];
	}

	// TESTS ------------------------------------------------

	/**
	 * @covers create
	 * @covers find
	 */
	public function testCreate(){
		list($i, $n) = $this->makeRandomCloud('Create');

		$c = $this->object->find($n);
		$this->assertEquals($i, $c->id);
	}

	// covered in create!
	//public function testFind(){
	//}

	public function testGet(){
		list($i, $t) = $this->makeRandomCloud('In');

		$c = $this->object->get($i);
		$this->assertNotNull($c);
		$this->assertEquals($t, $c->name);
	}

	public function testAll(){
		list($i, $t) = $this->makeRandomCloud('Id');

		$arr = $this->object->all();
		$this->assertNotNull($arr);
		$this->assertInternalType('array', $arr);
		$this->assertGreaterThan(0, count($arr));
	}

	public function testUpdate(){
		list($i, $t1) = $this->makeRandomCloud('Update');
		$t2 = 'UnitTest-CloudUpdate-'.rand(0, 9999);

		$b = $this->object->update($i, $t2);
		$this->assertTrue($b);

		$c2 = $this->object->find($t2);
		$this->assertEquals($i, $c2->id);
	}

	public function testDelete(){
		list($i, $t) = $this->makeRandomCloud('Delete');

		$b = $this->object->delete($i);
		$this->assertTrue($b);

		$arr = $this->object->find($t);
		$this->assertNull($arr);
	}

}
