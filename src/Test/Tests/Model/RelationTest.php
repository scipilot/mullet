<?php
namespace Scipilot\Mullet\Test\Tests\Model;

use Scipilot\Mullet\Model\Cloud;
use Scipilot\Mullet\Model\Relation;
use Scipilot\Mullet\Model\Tag;
use Scipilot\Mullet\Test\Mocks\TestContainer;
use Scipilot\Mullet\Test\Tests\BaseTest;

/**
 */
class RelationTest extends BaseTest {
	/**
	 * @var Relation
	 */
	protected $object;

	protected $cloudModel;
	protected $cloudId;
	/** @var  Tag */
	protected $tagModel;
	protected $tagId;

	/**
	 */
	protected function setUp(){
		//parent::setup(); I want to override the container!

		$this->app 		= new TestContainer();
		$this->tagModel = new Tag($this->app);
		$this->cloudModel = new Cloud($this->app);
		$this->object = new Relation($this->app, $this->tagModel, $this->cloudModel);

		// Create a new cloud
		$t = 'UnitTestCloud-'.rand(0,9999);
		// have to create a cloud first
		$this->cloudModel = new Cloud($this->app);
		$this->cloudId = $this->cloudModel->create($t);
		// DevNote: asserting in setup causes any 'i'ncomplete tests to be marked as passed!
		$this->assertNotNull($this->cloudId);
		$this->assertGreaterThan(0, intval($this->cloudId), "c=".$this->cloudId);
	}

	protected function makeTestSubject(){
		// TODO: Implement makeTestSubject() method.
	}

	/**
	 */
	protected function tearDown(){
		$this->object = null;
	}

	private function makeRandomTag($name){
		$t = 'UnitTest-Tag'.$name.'-'.rand(0, 9999);

		$i = $this->tagModel->create($t, $this->cloudId);
		$this->assertGreaterThan(0, intval($i));

		return [$i, $t];
	}

	// TESTS ------------------------------------------------

	/**
	 * @covers create
	 * @covers find
	 */
	public function testCreate(){
		list($i, $t) = $this->makeRandomTag('Create');
		$itemID = rand(0, 9999);

		$i = $this->object->create($t, $this->cloudId, $itemID);

		$b = $this->object->exists($t, $this->cloudId, $itemID);
		$this->assertTrue($b);
	}

	// covered in create!
	//	public function testExists(){
	//	}

	public function testItems(){
		// setup fixture
		list($i1, $t1) = $this->makeRandomTag('In');
		list($i2, $t2) = $this->makeRandomTag('In');
		$itemID1 = rand(0, 9999);
		$itemID2 = rand(0, 9999);
		$itemID3 = rand(0, 9999);
		$itemID4 = rand(0, 9999);

		$this->object->create($t1, $this->cloudId, $itemID1);
		$this->object->create($t1, $this->cloudId, $itemID2);
		$this->object->create($t2, $this->cloudId, $itemID3);
		$this->object->create($t2, $this->cloudId, $itemID4);

		// Test, positive and negative
		$a = $this->object->items($t1, $this->cloudId);
		$this->assertContains($itemID1, $a);
		$this->assertContains($itemID2, $a);
		$this->assertNotContains($itemID3, $a);
		$this->assertNotContains($itemID4, $a);

		$a = $this->object->items($t2, $this->cloudId);
		$this->assertContains($itemID3, $a);
		$this->assertContains($itemID4, $a);
		$this->assertNotContains($itemID1, $a);
		$this->assertNotContains($itemID2, $a);
	}

	public function testSave(){
		$this->markTestIncomplete();
	}

	public function testTags(){
		// setup fixture
		list($i1, $t1) = $this->makeRandomTag('Tags');
		list($i2, $t2) = $this->makeRandomTag('Tags');
		list($i2, $t3) = $this->makeRandomTag('Tags');
		list($i2, $t4) = $this->makeRandomTag('Tags');
		$itemID1 = rand(0, 9999);
		$itemID2 = rand(0, 9999);
		$this->object->create($t1, $this->cloudId, $itemID1);
		$this->object->create($t2, $this->cloudId, $itemID1);
		$this->object->create($t3, $this->cloudId, $itemID2);
		$this->object->create($t4, $this->cloudId, $itemID2);

		// Test, positive and negative
		$a = $this->object->tags($itemID1, $this->cloudId);
		$this->assertContains($t1, $a);
		$this->assertContains($t2, $a);
		$this->assertNotContains($t3, $a);
		$this->assertNotContains($t4, $a);

		$a = $this->object->tags($itemID2, $this->cloudId);
		$this->assertContains($t3, $a);
		$this->assertContains($t4, $a);
		$this->assertNotContains($t1, $a);
		$this->assertNotContains($t2, $a);
	}

	public function testDelete(){
		// setup fixture
		list($i, $t1) = $this->makeRandomTag('Delete');
		$itemID1 = rand(0, 9999);
		$this->object->create($t1, $this->cloudId, $itemID1);

		// Pre-test it created (to avoid false negative)
		$b = $this->object->exists($t1, $this->cloudId, $itemID1);
		$this->assertTrue($b);

		$b = $this->object->delete($t1, $this->cloudId, $itemID1);
		$this->assertTrue($b);

		$b = $this->object->exists($t1, $this->cloudId, $itemID1);
		$this->assertFalse($b);
	}

}
