<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\Config\IConfig;

class NullMockConfig implements IConfig {

	public function add($sKey, $sSubKey, $mValue, $bPersist = false) {
	}

	public function get($sKey, $bRefresh = false) {
	}

	public function set($sKey, $mValue, $bPersist=false) {
	}


}
