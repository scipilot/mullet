<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\Config\JsonFileConfig;
use Scipilot\Mullet\Storage\PdoDbStorage;

class TestContainer extends Container {

	function __construct(){
		//$this->config = new TestMockJsonFileConfig($this);
		$this->config = new JsonFileConfig($this);
		$this->log = new TestMockLog($this);
		$this->storage = new PdoDbStorage($this);
	}
}
