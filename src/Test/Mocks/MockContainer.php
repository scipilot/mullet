<?php
/**
 * Mock container - contains component mocks which don't do anything!
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\App\Container;

class MockContainer extends Container {

	function __construct(){
		$this->log = new MockLog();
		$this->config = new MockConfig();
		$this->storage = new MockStorage();
	}
}
