<?php
/**
 * Mock container - contains component mocks which don't do anything!
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\App\Container;

class NullMockContainer extends Container {

	function __construct(){
		$this->config = new NullMockConfig($this);
		$this->log = new NullMockLog($this);
		$this->storage = new NullMockStorage($this);
	}
}
