<?php
/**
 * Logs to the Tests/log folder.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\Log\LocalLog;

class TestMockLog extends LocalLog {

	function __construct() {
		self::$PATH_s = __DIR__.'/../log/pulse-%s.log';
	}
}
