<?php
/**
 * Ze storage, it does nothing!
 *
 * @author: scipilot
 * @since : 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\Storage\IStorage;

class NullMockStorage implements IStorage {

	public function create($entity, $rows) { }

	public function update($entity, $rows) {	}

	public function read($entity, $ids) {
		return array();
	}

	public function delete($entity, $ids) {	}

}
