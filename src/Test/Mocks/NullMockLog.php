<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\Log\ILog;

class NullMockLog implements ILog {

	public function setVerbosity($iVerbosity) {
	}

	public function write($sMessage, $iVerbosity=self::LOG_LEVEL_INFO) {
	}

}
