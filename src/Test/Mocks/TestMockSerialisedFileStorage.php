<?php
/**
 * Provides storage for the Unit Tests, in storage-UNITTEST.ser
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Mullet\Test\Mocks;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\Storage\SerialisedFileStorage;

class TestMockSerialisedFileStorage extends SerialisedFileStorage {

	function __construct(Container $appContainer) {
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../../storage/storage-UNITTEST.ser';
	}
}
