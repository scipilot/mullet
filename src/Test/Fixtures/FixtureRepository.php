<?php
namespace Scipilot\Mullet\Test\Fixtures;

/**
 * Fixture repository fetches the contents of fixture files for you.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
class FixtureRepository {

	public static function get($filename){
		return file_get_contents(__DIR__.'/'.$filename);
	}
}
