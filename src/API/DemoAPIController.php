<?php
/**
 * @author: Pip Jones
 * @since : 7/02/2015
 */
namespace Scipilot\Mullet\API;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\App\DefaultContainer;
use Scipilot\Mullet\Model\Cloud;
use Scipilot\Mullet\Model\Relation;
use Scipilot\Mullet\Model\Tag;

/**
 * EXAMPLE AJAX service API controller - for the demo site only.
 *
 * POST tag=T&cloud=Y                 Creates a tag
 * POST tag=T&item=X&cloud=Y          Relates a tag to an item (creating tag implicitly)
 * POST tags=T1,T2,...&item=X&cloud=Y Relates just these tags to an item (creating any tags implicitly)
 * POST cloud=Y                      Creates a cloud named Y
 *
 * GET item=X&cloud=Y                 Gets tags for an item
 * GET tag=T&cloud=Y                  Gets items with this tag
 * GET cloud=Y                        Gets all tags in a cloud
 *
 * To remove a tag from an item: you need to use 'POST tags' and not include the tag you are deleting.
 *
 * Ps. it's a bit RPCish as I couldn't be bothered to make multiple entry points per noun! (without a framework)
 *
 * @todo  delete single tag (can be done by 'tags' post currently)
 */
class DemoAPIController {
	/** @var  Container */
	protected $container;

	/**
	 * @todo: the model 'boot' is duplicated in the TagSearchViewModelFactory
	 *
	 * @param array $groups Array of group config arrays [ {'entity': 'ENTITY NAME', 'group': 'GROUP NAME' ,'table': "REL TABLE NAME"}, ... ]
	 */
	public function __construct($groups=null){
		$this->container = new DefaultContainer();

		// register any special entity tables
		if($groups) {
			foreach($groups as $group){
				$this->container->storage->group($group['entity'],$group['group'],$group['table']);
			}
		}

		// create the models
		$this->tagModel   = new Tag($this->container);
		$this->cloudModel = new Cloud($this->container);
		$this->relModel   = new Relation($this->container, $this->tagModel, $this->cloudModel);
	}

	/**
	 * Single entry point for all requests.
	 */
	public function route(){
		$response = (object)array('result' => false);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){

			// Get context
			$tags  = empty($_POST['tags']) ? null : $_POST['tags'];
			$tag   = empty($_POST['tag']) ? null : $_POST['tag'];
			$item  = empty($_POST['item']) ? null : $_POST['item'];
			$cloud = empty($_POST['cloud']) ? null : $_POST['cloud'];
			$group = empty($_POST['group']) ? null : $_POST['group'];
			$aTags = array();

			// tag 'noun' (includes empty 'tags' used for POSTing the delete last tag)
			if(isset($_POST['tags']) || $tag){
				// Tagging an item?
				if($item){
					// process multiple tags?
					if(isset($_POST['tags'])){
						$this->removeItemTags($cloud, $item, $group);
						$aTags = explode(',', $tags);
					}
					// unify single/multi tagging
					if($tag) $aTags = array($tag);
					if($aTags) $this->tagItems($aTags, $cloud, $item, $group);
				}
				else{
					$response = $this->createTag($tag, $cloud);
				}
			}
			// cloud 'noun'
			elseif($cloud){
				$response = $this->createCloud($cloud);
			}
		}
		else if($_SERVER['REQUEST_METHOD'] == 'GET'){

			// Get context
			$tag   = empty($_GET['tag']) ? null : $_GET['tag'];
			$item  = empty($_GET['item']) ? null : $_GET['item'];
			$cloud = $_GET['cloud'];
			$group = empty($_GET['group']) ? null : $_GET['group'];

			if($item){
				$response = $this->getTags($item, $cloud, $group);
			}
			elseif($tag){
				$response = $this->getItems($tag, $cloud);
			}
		}

		header('content-type: text/json');
		echo json_encode($response);
	}

	/** Controller method: replace, relate [& create] */
	public function tagItems($tags, $cloud, $item, $group=null){
		$response = (object)array('result' => true, 'debug' => '');

		foreach($tags as $tag){
			$r                = $this->tagItem($tag, $cloud, $item, $group);
			$response->result = $response->result && $r->result;
			$response->debug .= $r->debug;
		}
		return $response;
	}

	/** helper? function, deletes all tags before a bulk add.
	 *
	 * @param int $cloud
	 * @param int $item
	 *
	 * @return bool success
	 */
	protected function removeItemTags($cloud, $item, $group=null){
		return $this->relModel->delete(null, $cloud, $item, $group);
	}

	/** Controller method: relate [& create] */
	public function tagItem($tag, $cloud, $item, $group=null){
		$bResult  = true;
		$sMessage = ''; //debug

		// Option 1: just create (it doesn't fail on re-create)
		//$bResult = $this->tagModel->create($tag, $cloud);
		// Option 2: check first
		if(!$this->tagModel->in($tag, $cloud)){
			$bResult = $this->tagModel->create($tag, $cloud);
			$sMessage .= 'Created tag!';
		}

		if($bResult){
			if(!$this->relModel->exists($tag, $cloud, $item, $group)){
				$bResult = $this->relModel->create($tag, $cloud, $item, $group);
			}
			else{
				$sMessage .= 'Already tagged!';
			}
		}
		return (object)array('result' => $bResult, 'debug' => $sMessage);
	}

	/** Controller method */
	public function createTag($tag, $cloud){
		return (object)array('result' => $bResult = $this->tagModel->create($tag, $cloud));
	}

	/** Controller method */
	public function createCloud($cloud){
		return (object)array('result' => $this->cloudModel->create($cloud));
	}

	/** Controller method */
	public function getTags($item, $cloud, $group=null){
		if($item){
			$tags = $this->relModel->tags($item, $cloud, $group);
		}
		else{
			$tags = $this->cloudModel->tags($cloud);
		}
		return $tags;
	}

	/** Controller method */
	public function getItems($tag, $cloud, $group=null){
		return $this->relModel->items($tag, $cloud, $group);
	}
}
