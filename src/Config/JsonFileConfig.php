<?php
/**
 * Serialised file implementation of the configuration interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Mullet\Config;

use Scipilot\Mullet\App\Container;

class JsonFileConfig extends FileConfig {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../storage/config.json';
	}

	protected function decode($enc) {
		return json_decode($enc);
	}

	protected function encode($dec) {
		$j = json_encode($dec);
		return $j;
	}
}
