<?php
/**
 * Serialised file implementation of the configuration interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Mullet\Config;

use Scipilot\Mullet\App\Container;

class SerialisedFileConfig extends FileConfig {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../storage/config.ser';
	}

	protected function decode($enc) {
		return unserialize($enc);
	}

	protected function encode($dec) {
		return serialize($dec);
	}

}
