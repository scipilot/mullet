<?php
/**
 * A simple local log implementation, writes to 'storage/pulse-<DATE>.log'
 *
 * @author: scipilot
 * @since: 6/12/2014
 */

namespace Scipilot\Mullet\Log;


class LocalLog implements ILog {
	static $PATH_s = '';
	protected $iLogVerbosity = ILog::LOG_LEVEL_INFO;

	function __construct() {
		self::$PATH_s = __DIR__.'/../../storage/mullet-%s.log';
	}

	public function setVerbosity($iVerbosity){
		$this->iLogVerbosity = $iVerbosity;
	}

	public function write($sMessage, $iVerbosity=self::LOG_LEVEL_INFO){
		if ($this->iLogVerbosity >= $iVerbosity) {
			file_put_contents(sprintf(self::$PATH_s, date('Ymd')), date('H:i:s').' '.$sMessage."\n", FILE_APPEND);
		}
	}
}
