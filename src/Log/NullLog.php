<?php
/**
 * A null logger which does nothing.
 *
 * @author scipilot
 * @since 09/03/2016
 */
namespace Scipilot\Mullet\Log;

class NullLog implements ILog {
	public function setVerbosity($iVerbosity){}
	public function write($sMessage, $iVerbosity=self::LOG_LEVEL_INFO){}
}
