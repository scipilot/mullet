<?php
namespace Scipilot\Mullet\Model;

use Scipilot\Mullet\App\Contained;

/**
 * Clouds group tags into separate sets, enabling the use of multiple independent clouds in one application.
 *
 * Pattern: Model, Table Gateway
 *
 * @author: pipjones
 * @since : 6/02/2015
 */
class Cloud extends Contained {
	const STORAGE_ENTITY = 'cloud';
	/*
	 * Entity properties:
	 * 	id 	int 		Unique ID
	 * 	name string Cloud name
	 */

	/**
	 * @param int $name
	 *
	 * @return integer ID of tag created, or null for error.
	 */
	public function create($name){
		$id  = null;
		$ids = $this->container->storage->create(self::STORAGE_ENTITY, array(array('name' => $name)));
		//todo: handle duplicate error/warn? (we use replace so it's defensive)

		if($ids) $id = $ids[0];
		return $id;
	}

	/**
	 * Fetch a cloud by text name.
	 *
	 * @param int $name
	 *
	 * @return Object|null cloud DTO {id, name}, or null for not-found.
	 */
	public function find($name){
		$c = null;
		$cs =  $this->container->storage->read(self::STORAGE_ENTITY, array('name' => $name));

		if($cs) $c = $cs[0];
		return $c;
	}

	/**
	 * Fetch a cloud by ID.
	 *
	 * @param int $id Cloud ID
	 *
	 * @return Object|null cloud DTO {id, name}, or null for not-found.
	 */
	public function get($id){
		$c = null;
		$cs = $this->container->storage->read(self::STORAGE_ENTITY, array('id' => $id));

		if($cs) $c = $cs[0];
		return $c;
	}

	/**
	 * Fetches all Clouds.
	 *
	 * @return array|null (string $name, ...) on success; null on error; empty array if none found.
	 */
	public function all(){
		return $this->container->storage->read(self::STORAGE_ENTITY, null);
	}

	/**
	 * Updates the name of a group.
	 *
	 * @param int $id
	 * @param int $name New name
	 *
	 * @return       bool Success (update fails if not found)
	 */
	public function update($id, $name){
		$bSuccess = true;

		$aClouds = $this->container->storage->read(self::STORAGE_ENTITY, array('id' => $id));
		if(isset($aClouds[0])){
			$this->container->storage->update(self::STORAGE_ENTITY, array(array('id' => $aClouds[0]->id, 'name' => $name)));
		}
		else{
			// error not found
			$bSuccess = false;
			$this->container->log->write(sprintf('Error finding cloud %d to update to %s', $id, $name));
		}

		return $bSuccess;
	}

	/**
	 * Cascades to delete the group, all its tags and tag-item relationships
	 * (not the actual items of course!)
	 *
	 * @param int $id Cloud ID
	 *
	 * @return       bool Success
	 */
	public function delete($id){

		// delete relations
		$bSuccess = $this->container->storage->delete(Relation::STORAGE_ENTITY, array('cloud_id' => $id));
		if($bSuccess){
			// delete tags
			$bSuccess = $bSuccess && $this->container->storage->delete(Tag::STORAGE_ENTITY, array('cloud_id' => $id));
		}
		if($bSuccess){
			// delete cloud
			$bSuccess = $bSuccess && $this->container->storage->delete(Cloud::STORAGE_ENTITY, array('id' => $id));
		}

		return $bSuccess;
	}

	/**
	 * Gets all tags in a cloud.
	 *
	 * @param int $cloudId
	 *
	 * @return array|null (string $tag, ...), null on error, empty array on none found.
	 */
	public function tags($cloudId){
		$aoTags = $this->container->storage->read(Tag::STORAGE_ENTITY, array('cloud_id' => $cloudId));
		// flatten to string array
		$tags  = array_map(function ($oTag){ return $oTag->tag; }, $aoTags);
		return $tags;
	}

}
