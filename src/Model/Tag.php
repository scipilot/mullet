<?php
namespace Scipilot\Mullet\Model;

use Scipilot\Mullet\App\Contained;

/**
 * Models the tags.
 *
 * Pattern: Model, Table Gateway
 *
 * @author: pipjones
 * @since : 6/02/2015
 */
class Tag extends Contained {
	const STORAGE_ENTITY = 'tag';

	/*
	 * Entity properties:
	 * 	id		int Unique ID
	 * 	tag		string tag text
	 * 	cloud	int Cloud ID
	 */

	/**
	 * Create a new tag in a specific cloud.
	 *
	 * @param string $tag
	 * @param int    $cloud
	 *
	 * @return integer ID of tag created, or null for error.
	 */
	public function create($tag, $cloud){
		$id  = null;
		$ids = $this->container->storage->create(self::STORAGE_ENTITY, array(array('tag' => $tag, 'cloud_id' => $cloud)));
		//todo: handle duplicate error/warn? (we use replace so it's defensive)

		if($ids) $id = $ids[0];
		return $id;
	}

	/**
	 * Gets a tag from a cloud by name.
	 *
	 * It can be used to tests for existence or fetch the id.
	 *
	 * @param string $tag
	 * @param int    $cloudId
	 *
	 * @return Object tag DTO {id, tag}
	 */
	public function find($tag, $cloudId){
		$id  = null;
		$ids = $this->container->storage->read(self::STORAGE_ENTITY, array('tag' => $tag, 'cloud_id' => $cloudId));

		if($ids) $id = $ids[0];
		return $id;
	}

	/**
	 * Is a tag in a cloud?
	 *
	 * @param string $tag
	 * @param int    $cloud
	 *
	 * @return       bool
	 */
	public function in($tag, $cloud){
		return $this->container->storage->read(self::STORAGE_ENTITY, array('tag' => $tag, 'cloud_id' => $cloud)) != null;
	}

	/**
	 * Gets the ID of a tag.
	 *
	 * @param string $tag
	 * @param int    $cloud
	 *
	 * @return int|null ID
	 */
	public function id($tag, $cloud){
		$id = null;
		$t  = $this->container->storage->read(self::STORAGE_ENTITY, array('tag' => $tag, 'cloud_id' => $cloud));

		if($t && $t[0]) $id = $t[0]->id;
		return $id;
	}

	/**
	 * Changes the text of a tag, leaving all relations intact.
	 *
	 * @param string $tag
	 * @param int    $cloud
	 * @param string $new
	 *
	 * @return       bool
	 */
	public function update($tag, $cloud, $new){
		$bSuccess = true;

		$aTags = $this->container->storage->read(self::STORAGE_ENTITY, array('tag', 'cloud_id' => $cloud));
		if(isset($aTags[0])){
			$this->container->storage->update(self::STORAGE_ENTITY, array(array('id' => $aTags[0]->id, 'tag' => $new, 'cloud_id' => $cloud)));
		}
		else{
			// error not found
			$bSuccess = false;
			$this->container->log->write(sprintf('Error finding tag %s/%d to update to %s', $tag, $cloud, $new));
		}

		return $bSuccess;
	}

	/**
	 * @param int $id ID
	 * @param int $cloud
	 *
	 * @return       bool Success
	 */
	public function delete($id, $cloud){
		return $this->container->storage->delete(self::STORAGE_ENTITY, array('id' => $id, 'cloud_id' => $cloud));
	}
}
