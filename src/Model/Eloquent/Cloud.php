<?php
/**
 * @author    pipjones
 * @since     17/12/2015
 */
namespace Scipilot\Mullet\Model\Eloquent;

use \Illuminate\Database\Eloquent\Model;

/**
 * Tag Cloud model - Eloquent implementation.
 *
 * @property int id
 * @property string name
 */
class Cloud extends Model {

	protected $table = 'mullet_cloud';
	protected $primaryKey = 'id';
	protected $guarded = array('*');
	protected $visible = array('*');
	public $timestamps = false;
}
