<?php
/**
 * @author    pipjones
 * @since     17/12/2015
 */
namespace Scipilot\Mullet\Model\Eloquent;

use \Illuminate\Database\Eloquent\Model;

/**
 * Tag model - Eloquent implementation.
 *
 * @property int id
 * @property string tag
 * @property int cloud_id
 */
class Tag extends Model {

	protected $table = 'mullet_tag';
	protected $primaryKey = 'id';
	protected $guarded = array('*');
	protected $visible = array('*');
	public $timestamps = false;
}
