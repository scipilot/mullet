<?php
namespace Scipilot\Mullet\Model;

use Scipilot\Mullet\App\Contained;
use Scipilot\Mullet\App\Container;

/**
 * Models the Tag-Item many-to-many relationship.
 *
 * Pattern: Model, Table Gateway
 *
 * @todo  I'm questioning the use of explicit IDs rather than natural key of the tag itself. a) it's storage specific, b) it's for performance. Should it be handled by a data mapper?
 * @todo  Why is the cloud Id in the REL? it's in the item so they're unique anyway.
 *
 * @author: pipjones
 * @since : 06/02/2015
 */
class Relation extends Contained {
	const STORAGE_ENTITY = 'relation';
	/*
	 * Entity properties:
	 * 	tag_id	int Unique ID
	 * 	item_id 	int Unique Hash/ID
	 * 	cloud_id 	int Cloud ID
	 */

	/**
	 * @var Tag
	 */
	protected $tag;

	/**
	 * @var Cloud
	 */
	protected $cloud;

	/**
	 * @param Container $container IoC container
	 * @param Tag       $tag
	 * @param Cloud     $cloud
	 */
	public function __construct($container, $tag, $cloud){
		parent::__construct($container);
		$this->tag   = $tag;
		$this->cloud = $cloud;
	}

	/**
	 * Tags an item, implicitly creating the tag.
	 *
	 * @param string $tag
	 * @param int    $cloudId
	 * @param int    $item External Item ID 	(e.g. PK or GUID)
	 * @param string $type (Optional) External Item Type (e.g. Model/Entity Class like 'vehicle')
	 *
	 * @return bool Success
	 */
	public function create($tag, $cloudId, $item, $type=null){
		$t = $this->tag->find($tag, $cloudId);
		if(!$t){
			$this->tag->create($tag, $cloudId);
			$t = $this->tag->find($tag, $cloudId);
		}
		$ids = $this->container->storage->create(
				self::STORAGE_ENTITY,
				array(array('tag_id' => $t->id, 'cloud_id' => $cloudId, 'item_id' => $item)),
				$type ?: 'default'
		);

		return isset($ids);
	}

	/**
	 * Checks if an item is already tagged.
	 *
	 * @param string $tag
	 * @param int    $cloudId
	 * @param int    $itemId External Item ID
	 * @param string $type (Optional) External Item Type (e.g. Model/Entity Class like 'vehicle')
	 *
	 * @return bool
	 */
	public function exists($tag, $cloudId, $itemId, $type=null){
		$bExists = false;
		$t       = $this->tag->find($tag, $cloudId);
		if($t){
			$bExists = $this->container->storage->read(
							self::STORAGE_ENTITY,
							array('tag_id' => $t->id, 'cloud_id' => $cloudId, 'item_id' => $itemId),
							$type ?: 'default')
				!= null;
		}
		return $bExists;
	}

	/**
	 * Finds all items tagged with a tag.
	 *
	 * @see tags()
	 *
	 * @param string $tag (Optional; empty to get all items tagged by anything in this cloud.)
	 * @param int    $cloudId
	 * @param string $type (Optional) External Item Type (e.g. Model/Entity Class like 'vehicle')
	 *
	 * @return array [int $item ID, ...], null on error, empty array on none found.
	 */
	public function items($tag, $cloudId, $type=null){
		$rels = array();

		$t = $this->tag->find($tag, $cloudId);
		if($t){
			$rels = $this->container->storage->read(
					self::STORAGE_ENTITY,
					array('tag_id' => $t->id, 'cloud_id' => $cloudId),
					$type ?: 'default');
		}

		// lambda object_column() :-)
		$ids = array_map(function ($rel){ return $rel->item_id; }, $rels);
		return $ids;
	}

	/**
	 * Bulk Utility: replaces all current tag-items with the provided set.
	 *
	 * @todo not implemented
	 *
	 * @param array $tags
	 * @param int   $cloudId
	 * @param int   $item External Item ID
	 */
	public function save(array $tags, $cloudId, $item){
		trigger_error(__METHOD__ . ' NOT IMPLEMENTED!');
	}

	/**
	 * Gets tags associated with an item.
	 *
	 * @see items()
	 *
	 * @param int $itemId External Item ID
	 * @param int $cloudId
	 * @param string $type (Optional) External Item Type (e.g. Model/Entity Class like 'vehicle')
	 *
	 * @return array List of tags: [ string $tag, ...], null on error, empty array on none found.
	 */
	public function tags($itemId, $cloudId, $type=null){
		$tags = array();

		// join via relation
		$rels = $this->container->storage->read(Relation::STORAGE_ENTITY, array('cloud_id' => $cloudId, 'item_id' => $itemId), $type ?: 'default');
		$ids  = array_map(function ($element){ return $element->tag_id; }, $rels);
		$aoTags = $this->container->storage->read(Tag::STORAGE_ENTITY, array('cloud_id' => $cloudId, 'id' => $ids));

		// Flatten to string array, lambda style array_column for objects
		if($aoTags) $tags  = array_map(function ($oTag){ return $oTag->tag; }, $aoTags);
		return $tags;
	}

	/**
	 * Note this does not cascade. From the Tag or Item perspective it is a 'remove'.
	 *
	 * @param string $tag    Optional; if not set, removes all tags from the item
	 * @param int    $cloudId
	 * @param int    $itemId External Item ID
	 * @param string $type (Optional) External Item Type (e.g. Model/Entity Class like 'vehicle')
	 *
	 * @return bool Success
	 */
	public function delete($tag = null, $cloudId, $itemId, $type=null){
		$bSuccess = false;

		if($tag == null){
			$bSuccess = $this->container->storage->delete(
				self::STORAGE_ENTITY,
				array('cloud_id' => $cloudId, 'item_id' => $itemId),
				$type ?: 'default');
		}
		else{
			// lookup tag ID
			$id = $this->tag->id($tag, $cloudId);
			if($id){
				$bSuccess = $this->container->storage->delete(
					self::STORAGE_ENTITY,
					array('tag_id' => $id, 'cloud_id' => $cloudId, 'item_id' => $itemId),
					$type ?: 'default');
			}
		}

		return $bSuccess;
	}
}
