<?php
/**
 * Default set of components: Json config, local log, PDO storage.
 * Use this if can't be bothered injecting your own component dependencies.
 *
 * @author: scipilot
 * @since: 06/02/2014
 */
namespace Scipilot\Mullet\App;

use Scipilot\Mullet\Config\JsonFileConfig;
use Scipilot\Mullet\Log\LocalLog;
use Scipilot\Mullet\Storage\PdoDbStorage;

class DefaultContainer extends Container {

	function __construct(){
		// bit worried about timing here: container isn't fully populated... do config first, then logs.
		$this->config = new JsonFileConfig($this);
		$this->log = new LocalLog();
		$this->storage = new PdoDbStorage($this);
	}

}
