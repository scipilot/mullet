<?php
/**
 * The base IoC container. Use this if you want to inject your own set of components, else see DefaultContainer.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Mullet\App;

use Scipilot\Mullet\Config\IConfig;
use Scipilot\Mullet\Log\ILog;
use Scipilot\Mullet\Storage\IStorage;

class Container {

	/**
	 * @var ILog
	 */
	public $log;

	/**
	 * @var IStorage
	 */
	public $storage;

	/**
	 * @var IConfig
	 */
	public $config;

}
