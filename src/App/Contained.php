<?php
/**
 * Simple base class for all items contained in the container providing circular reference.
 * Remember to call parent::__construct($container); in your __ctor
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Mullet\App;

class Contained {

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->container = $appContainer;
	}
}
