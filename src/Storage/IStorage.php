<?php
namespace Scipilot\Mullet\Storage;

/**
 * The interface for entity storage, e.g. in DB, file.
 *
 * Supports sub-types of entities to facilitate tagging client entity types in 3NF.
 *
 * Pattern: Strategy Interface
 *
 * @author: scipilot
 * @since : 06/01/2014
 */
interface IStorage {

	/**
	 * Optionally allows you to register a custom table to store specific sub-sets of entities.
	 * This is primarily intended for use to enable tagged items relations to be stored in specific tables,
	 * to facilitate 3NF i.e. if multiple item types are used together, a mixed relational table doesn't allow
	 * foreign keys with constraints, and requires further mappings to find the original item content tables.
	 *
	 * If you do not call this method, the default single tag table is used, and you will be required to
	 * provide composite item keys and perform any id/class transformation yourself.
	 *
	 * If you want to bypass this storage interface in your back-end application and directly use the tables,
	 * for optimisation or legacy integration reasons, then its likely you'll need to use this facility to register
	 * your relational tables which join to your content class tables.
	 *
	 * @todo the 'table' breaks the storage interface layer... although I'm only using PDO now, it's not generic,
	 * 				but it could be interpreted for other methods? e.g. redis filename, json filename, etc.
	 * @todo allow setting the column names and PK too?
	 *
	 *
	 * @param string $entity Storage Entity name: 'tag' \ 'relation' | 'cloud'
	 * @param string $group  Groups, class or shard of this entity to be stored in this specific table.
	 * @param string $table  Table name to store this entity group in.
	 */
	public function group($entity, $group, $table);

	/**
	 * Stores a new entity instance.
	 *
	 * The rows must not include the primary key field(s).
	 *
	 * @param string $entity Storage Entity name: 'tag' \ 'relation' | 'cloud'
	 * @param array  $rows   List of hashes (assoc array) of instances i.e. array(array('name'=>'foo', ...), ...)
	 * @param string $group  (Optional) Group, class or shard this entity is to be found in. See group()
	 *
	 * @return array|null List of IDs created; null on errors.
	 */
	public function create($entity, $rows, $group = 'default');

	/**
	 * Gets a list of entity instance hashes (assoc array).
	 *
	 * The lookup is done by whatever id fields you provide. If an id maps to an array, it matches all items in the list.
	 * Passing a null filler is a wildcard search - all items!
	 *
	 * e.g.
	 *  read('tag', array('id' => 1))              // finds item 1
	 *  read('tag', array('id' => array(1,2,3)))  // finds item 1,2,3
	 *
	 * @param string     $entity Entity name: 'tag' | 'relation' | 'cloud'
	 * @param array|null $ids    array (string name => int|array id, ...)  Unique Entity instance id(s) (single or composite key)
	 * @param string $group  (Optional) Group, class or shard this entity is to be found in. See group()
	 *
	 * @return array|null List of entities; null on error
	 */
	public function read($entity, $ids, $group = 'default');

	/**
	 * Stores an entity instance.
	 *
	 * The rows must include the primary key fields(s).
	 *
	 * @param string $entity Entity name: 'tag' \ 'relation' | 'cloud'
	 * @param array  $rows   List of hashes (assoc array) of instances i.e. array(array('id'=>1, 'name'=>'foo', ...), ...)
	 * @param string $group  (Optional) Group, class or shard this entity is to be found in. See group()
	 *
	 * @return       bool Success
	 */
	public function update($entity, $rows, $group = 'default');

	/**
	 * Deletes a list of entity instances.
	 *
	 * @param string $entity Entity name: 'tag' | 'relation' | 'cloud'
	 * @param array  $ids    array (string name => int id, ...)  Unique Entity instance id(s) (single or composite key)
	 * @param string $group  (Optional) Group, class or shard this entity is to be found in. See group()
	 *
	 * @return       bool Success
	 */
	public function delete($entity, $ids, $group = 'default');
}
