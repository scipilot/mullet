<?php
/**
 * @todo (this is basically ==Contained now it's not doing any thing else)
 *
 * @author: scipilot
 * @since: 6/12/2014
 */

namespace Scipilot\Mullet\Storage;

use Scipilot\Mullet\App\Container;

abstract class Storage implements IStorage {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;
	}
}
