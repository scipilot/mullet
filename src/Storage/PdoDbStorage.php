<?php
namespace Scipilot\Mullet\Storage;

use Scipilot\Mullet\App\Container;
use Scipilot\Mullet\Log\ILog;

/**
 * PDO DB implementation of the entity storage interface.
 *
 * Requires configuration (user/pass settings not needed in some DSNs):
 *
 *  storage.db.pdo.dsn:
 *  storage.db.pdo.username:
 *  storage.db.pdo.password:
 *  storage.db.pdo.database:        default 'sp_unittest' - also included in all DSNs
 *
 * DSN examples:
 *  MySQL Driver format DSN:  'mysql:host=127.0.0.1;dbname=sp_unittest;charset=utf8'
 *  Windows System DSN:        'odbc:DSN=unittest_dsn;Uid=unittest;Pwd=unittest'
 *  ODBC-MySQL format DSN:    'odbc:Server=127.0.0.1;Database=sp_unittest;Uid=unittest;Pwd=unittest;'
 *  SQLite:                  'sqlite:/opt/databases/sp_unittest.sq3'
 *
 * Stores Mullet entities in simple relational table structure:
 * e.g.
 *
 * CREATE TABLE `mullet_cloud` (
 * `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 * `name` varchar(255) NOT NULL DEFAULT ' ',
 * PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 * CREATE TABLE `mullet_tag` (
 * `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 * `tag` varchar(255) NOT NULL DEFAULT '',
 * `cloud_id` int(11) unsigned NOT NULL,
 * PRIMARY KEY (`id`),
 * KEY `cloud_id` (`cloud_id`),
 * CONSTRAINT `mullet_tag_ibfk_1` FOREIGN KEY (`cloud_id`) REFERENCES `mullet_cloud` (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 * CREATE TABLE `mullet_tag_rel` (
 * `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 * `item_id` int(11) unsigned NOT NULL,
 * `cloud_id` int(11) unsigned NOT NULL,
 *  PRIMARY KEY (`item_id`,`tag_id`,`cloud_id`),
 *  KEY `cloud_id` (`cloud_id`),
 *  KEY `tag_id` (`tag_id`),
 *  CONSTRAINT `mullet_tag_rel_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `mullet_tag` (`id`),
 *  CONSTRAINT `mullet_tag_rel_ibfk_2` FOREIGN KEY (`cloud_id`) REFERENCES `mullet_cloud` (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 * @author scipilot
 * @since  06/01/2014
 */
class PdoDbStorage extends Storage {

	/**
	 * @var \PDO
	 */
	protected $pdo;


	//protected $database = 'sp_unittest';//default - now in config
	protected $tables = array(
		'tag'      => array('default'=>'mullet_tag'),
		'cloud'    => array('default'=>'mullet_cloud'),
		'relation' => array('default'=>'mullet_tag_rel')
	);
	protected $dataFields = array(
		'tag'      => array('tag', 'cloud_id'),
		'cloud'    => array('name'),
		'relation' => array('tag_id', 'cloud_id', 'item_id')
	);
	protected $keyFields = array(
		'tag'      => array('id'),
		'cloud'    => array('id'),
		'relation' => array()
	);

	function __construct(Container $appContainer){
		parent::__construct($appContainer);
	}

	// Loads late to allow config/container overrides.
	private function load(){
		// singleton initialisation
		if(empty($this->pdo)){
			// Get config
			$confDSN = $this->app->config->get('storage.db.pdo.dsn');
			$username = $this->app->config->get('storage.db.pdo.username');
			$password = $this->app->config->get('storage.db.pdo.password');

			try {
				$this->pdo = new \PDO($confDSN, $username, $password);
			}
			catch(\PDOException $Exception){
				$this->handleError(sprintf('%s Exception connecting: "%s" code:%d', __METHOD__, $Exception->getMessage(), (int)$Exception->getCode()));
			}
		}
	}

	public function group($entity, $group, $table){
		$this->tables[$entity][$group] = $table;
	}


	public function create($entity, $rows, $group='default'){
		$aIDs     = array();
		$bSuccess = true;
		$sError   = '';

		$this->load();

		// REPLACE won't error on re-create, but requires unique columns to work. (DEFENSIVE)
		$sqlFields      = implode(',', $this->dataFields[$entity]);
		$sqlPlacehoders = implode(',', array_fill(0, count($this->dataFields[$entity]), '?'));
		$sql            = 'REPLACE INTO ' . $this->tables[$entity][$group] . ' (' . $sqlFields . ') VALUES (' . $sqlPlacehoders . ')';

		if(($st = $this->pdo->prepare($sql)) !== false){
			foreach($rows as $row){
				// todo: Migrate to named placeholders: the order is implicit due to array_values and the keys are ignored!
				if($st->execute(array_values((array)$row))){
					$aIDs[] = $this->pdo->lastInsertId();
				}
				else{
					$bSuccess = false;
					$sError   = var_export($st->errorInfo(), true);
					break;
				}
			}
		}
		else{
			$bSuccess = false;
			$sError   = var_export($this->pdo->errorInfo(), true);
		}

		if(!$bSuccess){
			$this->handleError(sprintf('%s ERROR creating entity: %s. PDO said: %s', __METHOD__, $entity, $sError));
		}

		return $aIDs;
	}

	public function update($entity, $rows, $group='default'){
		$bSuccess = true;
		$sError   = '';
		$this->load();

		// Include key field(s) in the update query
		// Replace won't error if the item doesn't exist. (DEFENSIVE)
		$aFields        = array_merge($this->keyFields[$entity], $this->dataFields[$entity]);
		$sqlFields      = implode(',', $aFields);
		$sqlPlacehoders = implode(',', array_fill(0, count($aFields), '?'));
		$sql            = 'REPLACE INTO ' . $this->tables[$entity][$group] . ' (' . $sqlFields . ') VALUES (' . $sqlPlacehoders . ')';

		if(($st = $this->pdo->prepare($sql)) !== false){
			foreach($rows as $row){
				if(!$st->execute(array_values((array)$row))){
					$bSuccess = false;
					$sError   = var_export($st->errorInfo(), true);
					break;
				}
			}
		}
		else{
			$bSuccess = false;
			$sError   = var_export($this->pdo->errorInfo(), true);
		}

		if(!$bSuccess){
			$this->handleError(sprintf('%s ERROR storing entity: %s. PDO said: %s', __METHOD__, $entity, $sError));
		}

		return $bSuccess;
	}

	public function read($entity, $ids, $group='default'){
		$row      = null;
		$bSuccess = true;
		$sError   = '';
		$aValues  = array(); // declare for call by ref.
		$sqlWhere = '';

		$this->load();

		$aFields   = array_merge($this->keyFields[$entity], $this->dataFields[$entity]);
		$sqlFields = implode(',', $aFields);
		if($ids) $sqlWhere = $this->formatWhere($ids, $aValues);
		$sql = 'SELECT ' . $sqlFields
			. ' FROM ' . $this->tables[$entity][$group]
			. ($sqlWhere ? ' WHERE ' . $sqlWhere : '');

		if(($st = $this->pdo->prepare($sql)) !== false){
			if($st->execute($aValues)){
				$row = $st->fetchAll(\PDO::FETCH_OBJ);
			}
			else{
				$bSuccess = false;
				$sError   = var_export($st->errorInfo(), true);
			}
		}
		else{
			$bSuccess = false;
			$sError   = var_export($this->pdo->errorInfo(), true);
		}

		if(!$bSuccess){
			$this->handleError(sprintf('%s ERROR reading entity: %s. PDO said: %s', __METHOD__, $entity, $sError));
		}

		return $row;
	}

	public function delete($entity, $ids, $group='default'){
		$bSuccess = true;

		$aPlaceholders = array(); // declare for call by ref.

		$this->load();

		$sqlWhere = $this->formatWhere($ids, $aPlaceholders);
		$sql      = 'DELETE '
			. ' FROM ' . $this->tables[$entity][$group]
			. ' WHERE ' . $sqlWhere;

		$st = $this->pdo->prepare($sql);
		if($st->execute($aPlaceholders)){
			//
		}
		else{
			$bSuccess = false;
			$this->handleError(sprintf('%s ERROR reading entity: %s. PDO said: %s', __METHOD__, $entity, var_export($st->errorInfo(), true)));
		}

		return $bSuccess;
	}

	/**
	 * @param array $aIDs    List of fields and value(s) to match on
	 * @param array $aValues by reference return of placeholder values.
	 *
	 * @return string where clause with placeholders.
	 */
	protected function formatWhere($aIDs, &$aValues){
		$aWhere  = array();
		$aValues = array();

		foreach($aIDs as $name => $value){
			// list match?
			if(is_array($value)){
				// Manually make a where-in clause (as PDO can't bind arrays: http://stackoverflow.com/questions/920353/can-i-bind-an-array-to-an-in-condition)
				$aPlaceholders = array();
				foreach($value as $v){
					$aPlaceholders[] = '?';
					$aValues[]       = $v;
				}
				$aWhere[] = $name . ' IN (' . implode(',', $aPlaceholders) . ')';
			}
			else{
				// single match (I guess this could be done using IN too!)
				$aWhere[]  = $name . '= ?';
				$aValues[] = $value;
			}
		}
		return implode(' AND ', $aWhere);
	}

	private function handleError($sDebug){
		$this->app->log->write($sDebug, ILog::LOG_LEVEL_ERROR);
		//$this->app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_ERROR, $sDebug);
	}
}
