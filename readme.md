# Mullet - A Content Tagging Component

## Overview

Tagging is a powerful and flexible method to organise and find related data.

It can be used to mimic traditional one-to-many, many-to-one and many-to-many relations.
It can be used to implement categories, or folders, without the limitations of a tree structure.
It's great for content management, social media and messy semantic data which defy normalisation.

This library presents a configurable way to implement a content tagging system.

Its flexibility stems from a strategy injection pattern. This allows you to customise or interface each
functional component (storage, logging, config, api) as much or as little as you need.

- At the core is a data model of tags, clouds, relations and items.
- This is accessed by a simple entity gateway pattern and persisted underneath by PDO, JSON or serialised file storage.
- On top of that is an (example) AJAX API, (which is almost production ready if you're not fussy).
- Consuming that API is an (example) front-end UI using TagManager by Max Favilli.
- A sample Bootstrap page provides a live demo of that entire stack.

Everything except the model can be replaced with your own implementation. (Replace the model and there's nothing left!)

Here is a UML representation. (The many-to-many item-tag relationship is actually implemented in a class.)
![UML](doc/img/mullet-uml.png)


And the name? Well it's kind-of-related. "Word clouds are the mullets of the internet" - Jeffrey Zeldman

## Installation

Standard composer installation (or more likely you will fork it and require your own).

    > composer require scipilot/mullet

Then install the sample storage (e.g. PDO DB tables), set the configuration.
See the PDO Driver (src/Storage/PdoStorage.php) for CREATE schema.

Make the scipilot/mullet/storage folder writable to enable logging.

If you want, setup the demo homepage and demo api.

The demo page is entirely standalone with bootstrap etc, if you want to top-and-tail it,
 to integrate with your front-end framework, just retain the tagmanager and twitter typeahead js/css includes,
 the main container HTML and of course the demo inline Javascript JQuery bindings and event handlers.

For the demo API, you will need to provide the entry point depending on your vhost setup e.g.
 - either php-include the api/tag/index.php entry-point file,
 - or path the ajax directly to the vendor/scipilot/mullet/api folder (only possible if you shamefully admit your vendor libs are in the web root)
 - or implement your own demo api entry point script, simply three lines:

    require_once(__DIR__.'/ ... /vendor/autoload.php');
    $c = new Scipilot\Mullet\API\DemoAPIController();
    $c->route();

## Usage

There are three main use cases:

1. Content: consumption/presentation. This might be an internal process or backend integration.

2. User: who is doing the tagging, e.g. a content editor, business operator or joe public (if you allow that)

3. Admin: an optional tag manager/poweruser (if the tags are locked)

For the content use-case, you would use the `Model` classes to lookup content by tags.
If this isn't performant enough for you, accessing the DB storage direct or using your own DAL is your choice.
In this case, this library isn't offering you much beyond a suggested table structure, and admin UI.

For the second and third use-cases you would need to implement a front-end UI using something like the suggested
Bootstrap + Twitter Typeahead + TagManager combo via an AJAX API. So I built the demo of these just to show how to
go about it. The demo isn't intended to be part of this library, as you will undoubtedly have your own framework.

In future I (or others) might write plugins to support popular frameworks, such as a Laravel Package for the API.

### Usage Examples

See `demo/`, if you make this folder visible to the web you can play with most features.

Also see the Unit Tests for usages (although I've always hated reading people say that).

## Config

Configuration is stored in storage/config.EXT where EXT is the extension of the config strategy you chose.
By default this is JSON.

The PDO Storage strategy has the following config keys:

	"storage.db.pdo.dsn": "mysql:host=127.0.0.1;dbname=sp_unit_test;charset=utf8",
	"storage.db.pdo.database": "sp_mullet",
	"storage.db.pdo.username": "unittest",
	"storage.db.pdo.password": "unittest123"

That's all for the moment!

## Architecture

    - IoC App Container
        - Config DI
        - Storage DI
            - PDO
        - Log DI
    - Model
        - Tag
        - Cloud
        - Relation
    - API
        - Demo Ajax Endpoint
    - Demo Homepage UI
        - Bootstrap
        - JQuery (DOM bindings, events, ajax)
        - Tag UI
        - Predictive UI

See devnotes for initial development thoughts and ramblings.

### Roadmap

- Eloquent Storage strategy
- Optimisations (e.g. single-query joins)
- Road test in an enterprise integration (content backend via direct DB, with this demo as the admin front-end)
- Proper remote API (e.g. presented as TagManagerAPI, as it's pretty specific, or a REST endpoint)
- JSON Storage strategy, Serialised Storage strategy (Am i *really* going to write a rel-db?)
- Stemming (i.e. depluralisation)
- Machine/Triple Tags support (i.e. understanding predicates in searches)

TODO:

- PDO driver injection into PDOStorage (so an app can provide a DB connection from its app/config)
- Enforce locked clouds (currently front-end)

## Tests

There are currently unit-tests for: Model, Storage (PDO), Config and several mocks.

# Licence

This library is licenced under the LGPL v3.

Simply put: you can use this software pretty much as you wish as long as you retain this licence,
make the source available and don't blame me for anything.

I'd also really like to see any changes / fixes / suggestions - thanks!
