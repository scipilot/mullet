# Development notes for Tag Manager (Codename Mullet)

Devnotes are my ramblings, notes and journal of discovery while building this component.
I may delete them when it gets to v1.00.00.

---

# Initial Design

## Use cases

1) Managing Tags
This can be integrated into 2) or have admin/user split (locked tags).

- Requires CRUD on Tag and Cloud entities.
- Wants integration into existing entities for auto-tag-generation (cross-entity tagging)

2) Tagging entities
A user is tagging actual items with either free or locked tag modes.

- Requires CRUD on Rel
- Requires CRUD on Tag (free mode)
- Wants predictive text option, for tag entry
- Wants tag-cloud option, for tag entry
- Wants nice visualisation of tags.

3) Content relations via tags.
Once the entities are tagged, various other processes will of course need to use the relationships.

One of the aims of this component is to be flexible, thus the storage system is an injected strategy.
This has led to a neccessary abstraction of the identity of the entities, in the form of a uniform hash index,
regardless of storage strategy (json, serialised, DB etc).
This hash is part of this component, and therefore propably not stored in the application database close to the
entities, making tight integration difficult.

In theory, it's possible to use this component as the single-point of access for all tagging requirements,
completely encapsulating the tag storage and relation indexing, however this won't be the most performant
or integral solution for enterprise applications.

If the storage strategy used is a typical relational database (i.e. the PDO storage), then the application will almost
certainly want to use the database directly to join the relational table in various complex queries and
will thus need to break the black-box component approach.
This is fine! The component is effectively used as an administrative UI.

- Requires Item lookup by tag(s)
- Requires Tag(s) lookup by Item(s)
- Wants Item lookup by items sharing tags (graph traversal)


## DB
- Tag table
 - tag id
 - tag
 - group

- Tag Rel table
- tag id
- item id *

- Group table (only if name/meta needed)
 - group id
 - name
 - acls/meta/etc


### Item ID and foreign entities

The system should support tagging various application entities, thus it would need a type/id pairing in the item id.
e.g. Tagging individial vehicles, building or people would require a composite key of entity type, and specific entity id.

There's a few ways to acheive this:

1. Pack a composite key into ItemId
  - this component provids an opaque blob storage (e.g. string or binary) for the item-id field
  - the application packs/unpacks whatever it needs to identify the entity type/instance.
  - implicit knowledge in the app of the packing format.

2. Use a single GUID in the tag-rel table, provide or assume another entity rel-table.
  - Item ID is atomic
  - The application would lookup the Item GUID in a [GUID | TYPE | ID] rel table to then fetch the item.
  - It's possible to do this in one join, if you join to all "mutually exclusive" entity tables or via CASE (I've done this before!)
  - Or the select is two-step - similar to the packing in 1. But would probably be slower - requiring another query.

3. Add a second field to our tag rel table.
  - Eliminates the need for un/packing nor a second query/join to a entity type table.
  - Bleeds application domain knowledge into this component. (possibly... entity type/instance pairs are very common?)
  - Would still need a full set of "mutually exclusive" joins to all entity tables. (only on generic search)
  - It complicates the API returns, as the list of IDs now becomes a list of pairs of IDs.

4. Provide an Item ID strategy injection.
  - Enables the component to perform the packing/unpacking
  - ? Might enable a tighter/more optimal system while retaining domain boundaries
  - I like the general idea, but I can't actually visualise the interface

5. Facilitate multiple tag tables with specific entity relations! allow the user to design this in their RDBMS
TODO DEFINE FURTHER...
    - Each entity has its own tagging table (relation table)
    - There can thus be Foreign Keys to the entitiy table PK.
    - The tag Id still refers back to the one core tag definition table.
    - WHAT is the relationship between entity-tag table and cloud? I guess, undefined to keep it flexible?
        - Entities classes can still be in multiple clouds
        - Entity instances can also be tagged multiply in multiple clouds
        - Clouds can be shared across entities.
    - To perform the joins, the system needs to know the table name per entity class
      and thus know the entity class when performing any actions.
      This is the same addition to the API I didn't want to make in 3.
      But the tagging system does tag and relate entities - that's what it does!
    - Config will be needed to "register" the client entity tables.
      Unless it's done by convention.
    - OH DEAR, I've already used the word Entity in the storage interface! This will be confusing.
      TaggedEntity? ClientEntity? no. Taggee? Type/Item. Class/Object.
      Item is already in use. I'll go for Type/Item now - it could be refactored.

On reflection of 1-4, option 1) seems the best all round.
It impinges least on this component, and requires minimal processing in the calling application.

## Model
Tag
- C create tag
- R tag exists (util)
- R get tag(s)
- U edit tag
- D delete tag
Relation
- C add item-tag (implicit tag create)
- R item-tag exists (util)
- R get item-tags
- U save item-tags (bulk util)
- D remove item-tag
Utils
- caching?
- normalize tag name
- Item ID generation plugin (e.g. hash of your ID+Type)
- Auto generate tags from Items plugin
- Get tag-related tags (enable force directed graph!)

Item
- R get item IDs by tag

## Ajax

- Service
  - CRUD
- Predictive
- Response JSON/XML rendering (util)

## Front end

- Tag cloud of all tags (requires frequency count)
- Current item tags
- Input field for create, enter tag
- Predictive field for enter tag

Javascript
- Bootstrap?
- Drag drop

CSS

Try these frontends to design the pluggable UI:
- http://welldonethings.com/tags/manager/v3     - more recent, well thought out, bootstrap, demos bit buggy
- http://ioncache.github.io/Tag-Handler/        - well established, older.
- /motifcms/js/jquery/jquery.tagger.js          - One I integrated into a CMS before, older, v simple, requires other plugins.

Formitable integration field type

### Configuration Options

 - Item ID strategy
 - Tag repository plugin
 - Tag locking
 - Multiple clouds (groups)
 - Storage strategy


## Testing

phpunit-skelgen --test -- "\Scipilot\Mullet\Model\Tag" src/Model/Tag.php