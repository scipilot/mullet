<?php
require_once(__DIR__ . '/../vendor/autoload.php');

// Mini controller?
$itemSearchTags = empty($_REQUEST['item-search-tags']) ? null : $_REQUEST['item-search-tags'];
$itemId = empty($_REQUEST['item']) ? 1 : $_REQUEST['item'];
$cloudId = empty($_REQUEST['cloud']) ? 1 : $_REQUEST['cloud'];
// not used: $tags = empty($_REQUEST['tags']) ? array() : explode(',', $_REQUEST['tags']); //item search
$app = new Scipilot\Mullet\App\DefaultContainer();
$tagModel = new Scipilot\Mullet\Model\Tag($app);
$cloudModel = new Scipilot\Mullet\Model\Cloud($app);
$relModel = new Scipilot\Mullet\Model\Relation($app, $tagModel, $cloudModel);

$items = [];
$htmlItemSearchTags = '';
if($itemSearchTags){
	$items = $relModel->items($itemSearchTags, $cloudId);
	$htmlItemSearchTags = $itemSearchTags;
}

$jsCloudTagsArray = '[]';
$jsCloudTypeaheadValues = '[]';
$cloudTags = $cloudModel->tags($cloudId);
if ($cloudTags) {
	$jsCloudTagsArray      = renderJSTagManagerPrefillArray($cloudTags);
	$jsCloudTypeaheadValues = renderJSTypeaheadSourceArray($cloudTags);
}
else $htmlCloudTags = '[]';

$jsItemTagsArray = '[]';
$itemTags = $relModel->tags($itemId, $cloudId);
if ($itemTags) {
	$jsItemTagsArray       = renderJSTagManagerPrefillArray($itemTags);
	//$jsItemTypeaheadValues = renderJSTypeaheadSourceArray($itemTags);
}
else $jsItemTags = '[]';

$clouds = $cloudModel->all();

function renderJSTagManagerPrefillArray($aTags) {
	return '["' . implode('","', $aTags) . '"]';
}

function renderJSTypeaheadSourceArray($aTags) {
	return '["' . implode('","', $aTags) . '"]';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Business Model Tag Demo</title>

	<!-- Bootstrap core CSS -->
	<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	<link href="css/screen.css" rel="stylesheet">
	<link href="bower_components/tagmanager/tagmanager.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>

		/*typeahead*/
		.typeahead,
		.tt-query,
		.tt-hint {
			width: 396px;
			height: 30px;
			padding: 8px 12px;
			font-size: 24px;
			line-height: 30px;
			border: 2px solid #ccc;
			-webkit-border-radius: 8px;
			-moz-border-radius: 8px;
			border-radius: 8px;
			outline: none;
		}

		.typeahead {
			background-color: #fff;
		}

		.typeahead:focus {
			border: 2px solid #0097cf;
		}

		.tt-query {
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		}

		.tt-hint {
			color: #999
		}

		.tt-dropdown-menu {
			width: 422px;
			margin-top: 12px;
			padding: 8px 0;
			background-color: #fff;
			border: 1px solid #ccc;
			border: 1px solid rgba(0, 0, 0, 0.2);
			-webkit-border-radius: 8px;
			-moz-border-radius: 8px;
			border-radius: 8px;
			-webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
			-moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
		}

		.tt-suggestion {
			padding: 3px 20px;
			font-size: 18px;
			line-height: 24px;
		}

		.tt-suggestion.tt-cursor {
			color: #fff;
			background-color: #0097cf;
		}

		.tt-suggestion p {
			margin: 0;
		}
	</style>
</head>

<body>

<div class="container">

	<div class="starter-template">
		<h1>Business Model Tag Live Demo</h1>
		<p> For an overview see the official readme at <a href="https://bitbucket.org/scipilot/mullet">https://bitbucket.org/scipilot/mullet</a> </p>

		<h2>Live Demo Setup</h2>
		<p>You may enter any random content Item IDs (they are not interpreted by this component).</p>
		<p>The cloud must be created first! (see below)</p>
		<form>
			<label>Content Item #<input type="text" name="item" value="<?=$itemId?>"></label>
			<label>Cloud
				<select name="cloud">
<?php foreach($clouds as $cloud){ ?>
					<option value="<?=$cloud->id?>" <?=($cloudId==$cloud->id ? 'selected':'')?>><?=$cloud->id?>. <?=$cloud->name?></option>
<?php } ?>
				</select>
			</label>
			<button type="submit">Update Demo</button>
		</form>

		<h2>Use Cases Demo</h2>
		<p>This section allows you to play with the various actors' use cases.</p>
		<p>Content: is content consumption/presentation. The actor here might be an internal process.</p>
		<p>User: is a user doing the tagging, e.g. a content editor, business operator or joe public if you allow that</p>
		<p>Admin: is an optional tag manager/poweruser (if the tags are locked, not free for all)</p>


		<h3>Content: View Tag Cloud</h3>

		<p>Cloud <code>#<?= $cloudId ?></code> contains the tags: <code><?= $jsCloudTagsArray ?></code>>
		<hr/>


		<h3 >Content: Get An Item's Tags</h3>

		<p>Item <code>#<?= $itemId ?></code> is tagged with: <code><?= $jsItemTagsArray ?></code>
		<hr/>


		<a name="get-items-by-tags"></a>
		<h3 >Content: Get Items By Tags</h3>

		<span id="itemTagsContainer"></span>
		<form method="post" action="#get-items-by-tags">>
			<span id="userSearchTagsContainer"></span>
			<input type="text" name="item-search-tags-input" placeholder="Tags" class="tm-search" autocomplete="off"/>
			<div><!-- todo: ? this button disappears when to the RHS of the input field. unless in a div -->
				<button type="submit">Search</button>
			</div>
		</form>

		<p>Items <code>#<?= implode(',', $items) ?></code> are tagged with: <code><?= $htmlItemSearchTags ?></code>
		<hr/>


		<h3 >User: Tag an Item - Predictive, Locked Tags</h3>

		<p>In this demo, the user is is tagging content but can only choose from existing tags.</p>
		<label for="tmt-user-locked">Tagging Item <code>#<?= $itemId ?></code></label>
		<span id="userLockedTagsContainer"></span>
		<input type="text" placeholder="Choose Tags to add to this item" id="tmt-user-locked"/>

		<h3 >User: Tag an Item - Predictive, Free Tags</h3>

		<p>In this demo, the user is allowed to create tags while tagging content (free mode).</p>
		<label for="tmt-user-locked">Tagging Item <code>#<?= $itemId ?></code></label>
		<span id="userFreeTagsContainer"></span>
		<input type="text" placeholder="Choose Tags to add to this item" id="tmt-user-free" autocomplete="off"/>
		<a onclick="$('#tm-typeahead').tagsManager('pushTag','I_am_a_new_tag');">add a preset tag from a link</a>
		<hr/>


		<h3 >Admin: Create New Tags</h3>
		<p>You can prepare a set of tags, without tagging any items. This works well with the locked tagging method.</p>
		<label for="tmt-user-locked">Administering Cloud <code>#<?= $cloudId ?></code></label>
		<span id="adminTagsContainer"></span>
		<input type="text" placeholder="Add tags to this cloud" id="tmt-admin"/>
		<hr/>
		<p>Todo: rename tags</p>
		<p>Todo: delete tags</p>

		<h3 >Admin: Create a New Cloud</h3>
		<label for="tf-cloud">Enter cloud name</label>
		<form onsubmit="$('#tf-cloud').trigger('mullet:createCloud', this); return false">
		<span id="adminTagsContainer"></span>
		<input type="text" placeholder="Enter new cloud name" name="cloud_name" id="tf-cloud" autocomplete="off"/>
		<button type="submit">Create cloud</button>
		</form>
		<p>todo: refresh page after form is posted (and ajax returns)</p>
		<hr/>

		<h3 >Admin: View Clouds</h3>
		<label for="tf-cloud">These are the current independent clouds</label>
			<ul>
<?php foreach($clouds as $cloud){ ?>
				<li><a href="?cloud=<?=$cloud->id?>"><code><?=$cloud->id?>. <?=$cloud->name?></code></a></li>
<?php } ?>
			</ul>
		<hr/>
		<p>Todo: rename clouds</p>
		<p>Todo: delete clouds</p>


		<h2>Demo Implementation Notes</h2>
		<p>This demo front-end is built using <a href="http://welldonethings.com/tags/manager/v3">Tag Manager v3</a>
			but that's not neccesarily required for this component - it's agnostic.
			However there is a sample API which supports Tag Manager's style and quirks (e.g. lack of tag delete ajax).
		</p>
		<p>TODO: implement a sample front-end in http://ioncache.github.io/Tag-Handler/ which should improve the robustness of the backend.
		</p>
		<p>TODO: I might make a real API, but it's hard to do so without committing to a framework. A controller helper might be useful?
		</p>


	</div>

</div>
<!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="assets/js/ie10-viewport-bug-workaround.js"></script>-->

<script src="bower_components/tagmanager/tagmanager.js"></script>
<script src="bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

<script>
	var jsItemTagsArray = <?=$jsItemTagsArray?>;
	var jsCloudTagsArray = <?=$jsCloudTagsArray?>;
	var jsCloudTypeaheadValues = <?=$jsCloudTypeaheadValues?>;

	$(document).ready(function () {

		var tmSearch = $(".tm-search").tagsManager({
			hiddenTagListName: 'item-search-tags',
			tagsContainer: '#userSearchTagsContainer',
			tagList: jsCloudTagsArray
		});
		tmSearch.typeahead({
				name: 'mulletapi',
				limit: 15,
				highlight: true
			},
			{
				name: "mulletapi",
				source: function (query, cb) {
					cb(filterTypeahead(query));
				}
			});

		var tmUserLocked = $("#tmt-user-locked").tagsManager({
			prefilled: jsItemTagsArray,
			tagsContainer: '#userLockedTagsContainer',
			AjaxPush: 'api/tag/',
			AjaxPushParameters: { 'authToken': 'todo', 'item': <?=$itemId?>, 'cloud':<?=$cloudId?> },
			/* todo: tagList & onlyTagList is not DRY and not scalable, see: https://github.com/max-favilli/tagmanager/issues/209*/
			onlyTagList: true,
			tagList: jsCloudTagsArray
		});
		tmUserLocked.typeahead({
				name: 'mulletapi',
				limit: 15,
				highlight: true
			},
			{
				name: "mulletapi",
				source: function (query, cb) {
					cb(filterTypeahead(query));
				}
			})
			.on('typeahead:selected', function (e, d) {
				console.log('typeahead:selected ');
				console.log(e);
				console.log(d);
				console.log('pushing tag...' + d.value + ' to tagApi' + tagApi);
				// this push includes the item id - it's pushing just the relationship
				tmUserLocked.tagsManager("pushTag", d.value);
			})
			.on('tm:invalid', onInvalidTag)
			.on('tm:pushing', onPushing)
			.on('tm:pushed', onPushed)
			.on('tm:popping', onPopping)
			.on('tm:popped', onPopped)
			.on('tm:duplicated', onDuplicated);

		var tmUserFree = $("#tmt-user-free").tagsManager({
			prefilled: jsItemTagsArray,
			tagsContainer: '#userFreeTagsContainer',
			AjaxPush: 'api/tag/',
			AjaxPushAllTags: true,// trial this, to do implicit delete/create all
			AjaxPushParameters: { 'authToken': 'todo', 'item': <?=$itemId?>, 'cloud':<?=$cloudId?>, 'mode': 'set'}
		});
		tmUserFree.typeahead({
				name: 'mulletapi',
				limit: 15,
				highlight: true
			},
			{
				name: "mulletapi",
				source: function (query, cb) {
					cb(filterTypeahead(query));
				}
			})
			.on('typeahead:selected', function (e, d) {
				console.log('typeahead:selected ');
				console.log(e);
				console.log(d);
				console.log(' pushing tag...' + d.value + ' to tagApi' + tagApi);
				// this push includes the item id - it's pushing the tag and the relationship (creates the tag implicitly)
				tmUserFree.tagsManager("pushTag", d.value);
			})
			.on('tm:invalid', onInvalidTag)
			.on('tm:pushing', onPushing)
			.on('tm:pushed', onPushed)
			.on('tm:popping', onPopping)
			.on('tm:popped', onPopped)
			.on('tm:duplicated', onDuplicated);

		var tagApi = $("#tmt-admin").tagsManager({
			prefilled: jsCloudTagsArray,
			tagsContainer: '#adminTagsContainer',
			AjaxPush: 'api/tag/',
			AjaxPushParameters: { 'authToken': 'todo', 'cloud':<?=$cloudId?> }
		});

		$('#tf-cloud').on('mullet:createCloud', onCreateCloud);

	}); //on-ready

	function filterTypeahead(char){
		var filteredTagList = [];
		for(var i in jsCloudTypeaheadValues){
			var a = jsCloudTypeaheadValues[i];
			if(a.indexOf(char) != -1){
				filteredTagList.push({value:a});
			}
		}
		return filteredTagList;
	}

	// TagManager event helpers
	function onInvalidTag(e, d) {
		console.log('tm:invalid tag...' + d + ' ');
		console.log(d);
	}
	function onPushing(e, d) {
		console.log('tm:pushing tag...' + d + ' ');
		console.log(d);
	}
	function onPushed(e, d) {
		console.log('tm:pushed tag...' + d + ' ');
		console.log(d);
	}
	function onPopping(e, d) {
		console.log('tm:popping tag...' + d + ' ');
		console.log(d);
	}
	function onPopped(e, d) {
		console.log('tm:popped tag...' + d + ' ');
		console.log(d);
		console.log(e);
	}
	function onDuplicated(e, d) {
		console.log('tm:duplicated tag...' + d + ' ');
		console.log(d);
	}

	// Custom event helpers
	function onCreateCloud(e, d){
		console.log('mullet:createCloud ...' + d + ' ');
		console.log(d);
		$.ajax({
			url: 'api/tag/',
			type: 'POST', // stupid: it's METHOD JQ people...!
			data:{
				cloud: d.value
			}
		})
	}

</script>
</body>
</html>
