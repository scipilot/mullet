# Demo site

This is a sample page to demo the features of the component.

It's implemented in Tagmanager, Twitter Typeahead and Bootstrap 3.

However all of those are just examples (probably the best ones!), you could use any front-end UI.

The index file has some example direct uses of the model, and there's an example Ajax API controller.
Of course you will be building this in your framework of choice, so these are just quick examples
and are not meant for production use, nor even as a base to build on!

To use:

 - install the bower components in this demo folder
 - use the DefaultContainer, or make a variant with your strategies.
 - set up your storage (e.g. database)
 - edit storage/config.json

> The demo page runs straight from bower_components/xx/dist so there's no publishing required as long as you can reach your vendor lib from the web.
