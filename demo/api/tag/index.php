<?php
/**
 * Test/temp AJAX service API controller
 *
 * @author: pipjones
 * @since : 7/02/2015
 */
require_once(__DIR__ . '/../../../vendor/autoload.php');

use Scipilot\Mullet\API\DemoAPIController;

$c = new DemoAPIController();
$c->route();
